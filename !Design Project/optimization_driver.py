#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Dec  5 21:49:08 2020

@author: sebastiendevilliers
"""

# The purpose of this code is to perform optimization using a fairly accurate 
# model of a miniature train powered by compressed air. Details of the train's c
# onstruction and use can be found in the DesignProject.pdf for this assignment. 
# This model aims to optimize the design parameters listed in Table 2 of the 
# aforementioned pdf. Throughout this code, we have used the same variable names
# for these parameters.
#
# NOTE: All measurements are in SI units unless otherwise stated
# NOTE: hereafter, the words 'design parameters' refer to this specific set of parameters
# NOTE: When the design parameters are listed in an array they should be listed 
#       in the following order
#
# Parameters from Table 2 in DesignProject.pdf (SI units)
# L_t:        length of train
# r_o:        radius of tank
# rho_a_t:    density of train material
# P0_gauge:   initial tank gauge pressure
# r_g:        pinion gear radius 
# L_r:        length of piston stroke
# r_p:        radius of piston

# NOTE: this file is broken into segments to that you can 
# do the brute force, optimization, and displayResults results separately
# see the top of each segment for running instructions.

from objective_function import objective_func
import numpy as np
import scipy.optimize as optimize
from timeit import timeit

paramNames = np.array(['L_t', 'r_o', 'rho_t', 'P0_gauge', 'r_g', 'L_r', 'r_p'])

def displayResults(designParams):
    tf = objective_func(designParams, True)
    """
    Given a set of design parameters this function will display 
     - said parameters
     - the time it took for the train to finish the race
     - a graph of the train's motion
    """
    
    print("Final time", tf, "seconds")
    for i in range(len(designParams)):
        print("{:9}= {:<10.4g}".format(paramNames[i], designParams[i]))

#%% Brute Force Search -- run top cell & this cell

def bruteForce():
    """
    Optimize the design parameters via brute force search through 7*(5^6)
    combinations of possible design parameters.
    
    returns: nothing. displays the results when done
    """
    # timeitFun function handle is called once. Helps to time the brute force.
    def timeitFun():
        minParams = np.zeros(7)
        minTime = np.inf
        for L_t in np.linspace(0.2, 0.3, 5):
            for r_o in np.linspace(0.05, 0.115, 5):
                for rho_t in [1400,1200,7700,8000,4500,8940,2700]:
                    for P0_gauge in np.linspace(70000, 200000, 5):
                        for r_g in np.linspace(0.002, 0.01, 5):
                            for L_r in np.linspace(0.1, 0.5, 5):
                                for r_p in np.linspace(0.02, 0.04, 5):
                                    designParams = np.array([L_t, r_o, rho_t, P0_gauge, r_g, L_r, r_p])
                                    tf = objective_func(designParams)
                                    if tf < minTime:
                                        minTime = tf
                                        minParams = designParams
        
        displayResults(minParams)
        
    bruteForceTime = timeit(timeitFun, number=1)
    print('Calculation Took:', bruteForceTime, 'sec')
    print('Time Per Iteration:', bruteForceTime/(5**6*7), 'sec')

bruteForce()

# Last Output

# Final time 5.541989438111988 seconds
# L_t      = 0.2       
# r_o      = 0.09875   
# rho_t    = 8940      
# P0_gauge = 1.35e+05  
# r_g      = 0.008     
# L_r      = 0.5       
# r_p      = 0.02      
# Calculation Took: 218.44757309800025 sec
# Time Per Iteration: 0.0019972349540388593 sec

#%% Optimization -- run top cell & this cell

def optimization(L_t, r_o, rho_t, P0_gauge, r_g, L_r, r_p):
    """
    Uses scipy.optimize.differential_evolution to find the design parameters that
    minimize the finish time for the model train.
    
    paramters: the design parameters from brute force
    returns: nothing. displays results when done
    """
    def timeitFun():
        def fun(x):
            # ensure that rho_t is kept constant
            params = np.array([*x[:2], rho_t, *x[2:]])
            return objective_func(params)
        bounds = np.array([[0.2, 0.3], [0.05, 0.115], [70000, 200000], [0.002, 0.01], [0.1, 0.5], [0.02, 0.04]])
        res = optimize.differential_evolution(fun,bounds)
        tf = res['fun']
        optParams = np.array([*res['x'][:2], rho_t, *res['x'][2:]])
        print(res)
        displayResults(optParams)
    
    optimizationTime = timeit(timeitFun, number=1)
    print('Calculation Took:', optimizationTime, 'sec')

L_t      = 0.2       
r_o      = 0.09875   
rho_t    = 8940      
P0_gauge = 1.35e+05  
r_g      = 0.008     
L_r      = 0.5       
r_p      = 0.02  
latestBruteForce = np.array([L_t, r_o, rho_t, P0_gauge, r_g, L_r, r_p])
optimization(*latestBruteForce)

# Last Output

# Final time 5.555327086755485 seconds
# L_t      = 0.2544    
# r_o      = 0.06817   
# rho_t    = 8940      
# P0_gauge = 9.462e+04 
# r_g      = 0.004439  
# L_r      = 0.2963    
# r_p      = 0.02378   
# Calculation Took: 64.76272366400008 sec

#%% Results Only -- run top cell & this cell

# DESIGN PARAMETERS YOU WANT TO DISPLAY GO HERE
# Realistic Params...
L_t      = 0.2544
r_o      = 0.0635
rho_t    = 8000
P0_gauge = 7.5e+04 
r_g      = 0.004
L_r      = 0.3048    
r_p      = 0.0238 
latestOptimum = np.array([L_t, r_o, rho_t, P0_gauge, r_g, L_r, r_p])
displayResults(latestOptimum)