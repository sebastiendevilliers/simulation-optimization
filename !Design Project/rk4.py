import numpy as np

def rk4(odefun, t0, y0, transition, step=(0.25,0.01), maxiter=1000):
    """ Performs the 4th-Order Runge-Kutta Method to solve a system of ordinary
    differential equations with an arbitrary number of equations
    
    Parameters
    ----------
    
    transition : float
        the x-value where we switch from acceleration to deceleration
        i.e transition = L_r * r_w / r_g
    step : (float, float)
        stepsizes (normal, precise)
        
    Note
    ----
    This rk4 has been customized for the specific probem. 
    - It will stop calculating when t[k] = maxiter s or y[k,1] < 0
    - It will make the stepsize smaller (h=step[1]) for the first section
    """

    # Determine the number of items in the outputs
    num_steps = maxiter
    y0 = np.asarray(y0)
    num_states = y0.shape[0]
    h = step[1]

    # Initialize the outputs
    t = np.zeros(num_steps)
    y = np.zeros((num_steps, num_states))

    # Assign the first row of outputs
    t[0] = t0
    y[0,:] = y0[:]

    # Start the loop
    for k in range(num_steps-1):
        # if we've passed the transition point and haven't already switched...
        if y[k,0] - transition >= 0 and h != step[0]: 
            # switch to bigger steps
            h = step[0]

        # Calculate the slope
        k1 = odefun(t[k], y[k,:])
        k2 = odefun(t[k]+.5*h, y[k,:]+.5*k1*h)
        k3 = odefun(t[k]+.5*h, y[k,:]+.5*k2*h)
        k4 = odefun(t[k]+h, y[k,:]+k3*h)
        slope = (k1 + 2*k2 + 2*k3 + k4) / 6.
        
        # Calculate the next state
        y[k+1,:] = y[k,:] + slope * h
        t[k+1] = t[k]+h
        
        # if velocity drops below zero, stop calculating & return what we have
        if y[k+1,1] < 0: return t[:k+2], y[:k+2,:]
    
    # if we reached this point using the default step sizes and max iter
    # it means the train took over 200 sec to stop and the parameters were likely bad 
    # and objective_function will end up discarding the results
    print("Oops! Looks like iterations maxed out before finding zero velocity")
    print("maxiter = ",maxiter)
    return t, y
