#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Train Motion Function Modified from Lab9

Created on Tue Nov 24 23:27:35 2020
@author: sebastiendevilliers
"""
import numpy as np

class Error(Exception):
    """ Raised when the an error occurs in this file """
    pass

def train_motion(t, y, designParams, fixedParams, computedParams, ignoreErrs=False):
    """
    Evaluates the ODE's governing the train's motion. Switches from deceleration
    to acceleration when the piston has fully extended
    
    Parameters
    ----------
    t : float
        Current Time.
    y : float vector
        State [position, velocity] of the moving train at time t. 
    designParams : parameters from Table 2 in ReadMe/DesignProject.pdf
        L_t:        length of train
        r_o:        radius of tank
        rho_t:    density of train material
        P0_gauge:   initial tank gauge pressure
        r_g:        pinion gear radius 
        L_r:        length of piston stroke
        r_p:        radius of piston
    fixedParams : the constants from Table 1 plus a few others. see objective_function
    computedParams : see objective_function
    ignoreErrs : bool
        If True, will not raise wheel slip and end of track errors, prints them instead

    Returns
    -------
    float vector
        The result of evaluating the two f􏳉irst-order ODEs at time t. 
        [velocity, acceleration]

    """
    # see driver script for variable descriptions
    L_t, r_o, rho_t, P0_gauge, r_g, L_r, r_p = designParams
    rho_a, P_atm, C_d, C_r, mu_s, r_w, m_w, g = fixedParams
    wall, V0, V_t, A_p, L_p, m_p, m, A = computedParams
    
    # position, velocity
    x, v = y

    dxdt = v
    
    if y[0] < L_r * r_w / r_g:
        # propulsion stage  =>  m * dvdt = Ft - Fd - Fr
        dvdt = 1/(m + m_w) * (A_p*r_g/r_w * ((P0_gauge+P_atm)*V0/(V0 + A_p*r_g/r_w*x) - P_atm) - 0.5*C_d*rho_a*A*(v**2) - C_r*m*g)
        
    else:
        # deceleration stage  =>  m * dvdt = - Fd - Fr
        dvdt = 1/m * (-0.5*C_d*rho_a*A*(v**2) - C_r*m*g)
        
    # check for wheel slip
    Ft = r_g*P0_gauge*A_p/r_w - m_w*dvdt
    if Ft > mu_s*m/2*g: 
        wheelSlipErr = Error("Wheel slip occured!")
        if ignoreErrs: 
            print(wheelSlipErr)
            print('Continuing calculations anyway...')
        else:
            raise wheelSlipErr
    
    # check for the end of the track
    if x > 12.5:
        tooFarErr = Error("Train ran off the track!")
        if ignoreErrs:
            print(tooFarErr)
            print('Continuing calculations anyway...')
        else:
            raise tooFarErr
        
    dydt = np.array([dxdt, dvdt])
    return dydt