#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Objective Function
Created on Thu Nov 19 13:13:51 2020

@author: sebastiendevilliers
"""

import numpy as np
from train_motion import train_motion, Error as TrainMotionErr
from rk4 import rk4
# only used if displayResults=True
from plot_results import plot_results, interactive_plots
import traceback


class Error(Exception):
    """ 
    Raised when an error originates in this file 
    
    Note: To get more info use
    import traceback
    traceback.print_tb(err.__traceback__)
    ^ this is async tho
    
    """
    pass

def objective_func(params, displayResults=False, stepsize=0.25):
    """
    Solves the system of two f􏳉irst-order ODEs governing the train's motion
    Checks a bunch of restrictions as per DesignProject.pdf
    Outputs the time that the train crosses the finish line
    
    Parameters
    ----------
    params : parameters from Table 2 in ReadMe/DesignProject.pdf (SI units)
        L_t:        length of train
        r_o:        radius of tank
        rho_a_t:    density of train material
        P0_gauge:   initial tank gauge pressure
        r_g:        pinion gear radius 
        L_r:        length of piston stroke
        r_p:        radius of piston
    
    displayResults : bool
        plots a graph of the train's motion when true.
        will ignore some restrictions (prints warning) in order to get you that graph

    Returns
    -------
    tf : float
        The time that the train crosses the finish line. 
        Returns np.inf if restrictions were not met

    """
    
    L_t, r_o, rho_t, P0_gauge, r_g, L_r, r_p = params
    
    #
    # Table 1: Relevant Physical Parameters
    #
    
    rho_a = 1.0 # • Air density: 1.0 kg/m3
    P_atm = 101325 # • Atmospheric pressure Pa
    C_d = 0.8 # • Drag coef􏰁icient
    C_r = 0.03 # • Rolling resistance coef􏰁icient
    mu_s = 0.7 # • Coef􏰁icient of static friction
    r_w = 0.02 # • Wheel radius m
    m_w = 0.1 # • Mass of wheels and axels kg
    
    g = 9.81 # • Acceleration of gravity m/s^2 
    
    fixedParams = np.array([rho_a, P_atm, C_d, C_r, mu_s, r_w, m_w, g])
    
    #
    # Computed Properties
    #
    
    # Tank wall thickness = r_o - r_i (m)
    wall = r_o - (r_o / 1.15) 
    # Calculate V0 based on the inner tank diameter and length
    V0 = np.pi * (r_o - wall)**2 * (L_t - 2*wall)
    # Volume of tank material = outer cylinder - inner cylinder (kg)
    V_t = (np.pi*r_o**2 * L_t) - V0
    
    # Area of the piston
    A_p = np.pi * r_p**2
    # Length of the piston (m)
    L_p = 1.5*L_r
    # Mass of the piston (kg)
    m_p = 1250 * (np.pi * (r_p**2) * L_p)
    
    # Total train mass (kg)
    m = rho_t*V_t + m_p + m_w
    
    # Frontal area of train (m^2)
    A = np.pi*r_o**2 
    
    computedParams = np.array([wall, V0, V_t, A_p, L_p, m_p, m, A])
    
    # Check initial restrictions
    try:
        verifyParams(params, fixedParams, computedParams)
    except Error as err:
        if displayResults: 
            print(err)
            print("Continuing calculations anyway...")
        else:
            return np.inf
        
    
    #
    # Set Initial Conditions and other RK4 inputs
    #
    
    y0 = np.array([0,0]) # • Initial conditions on train: x0 = 0 and v0 = 0.
    tspan = np.append(np.arange(0,3,0.25), np.arange(3, 100, stepsize))
    
    def odefun(t, y):
        return train_motion(t, y, params, fixedParams, computedParams, ignoreErrs=displayResults)
    
    #
    # Integrate
    #
    
    try:
        tspan,result = rk4(odefun, 0, y0, (L_r * r_w / r_g))
    except TrainMotionErr:
        return np.inf
        
    position = result[:,0]
    velocity = result[:,1]
    
    if displayResults:
        plot_results(tspan, position, velocity, 10, 12.5-10, title="Train Motion")
    
    #
    # Find tf
    #
    
    # tf is when position = 10 m
    try: tf = np.interp(10, position, tspan)
    except Exception as err:
        if displayResults: print(str(err)+"\nUnable to find finish time.")
        return np.inf
        
    if max(position) < 10:
        if displayResults: print("Train did not finish the race")
        return np.inf
        
    # SUCCESS! return the time that it crossed the finish
    return tf
    
def verifyParams(designParams, fixedParams, computedParams):
    """
    Raises an Error if the Design Parameters are invalid

    Parameters
    ----------
    designParams : parameters from Table 2 in ReadMe/DesignProject.pdf (SI units)
        L_t:        length of train
        r_o:        radius of tank
        rho_t:      density of train material
        P0_gauge:   initial tank gauge pressure
        r_g:        pinion gear radius 
        L_r:        length of piston stroke
        r_p:        radius of piston

    """
    
    L_t, r_o, rho_t, P0_gauge, r_g, L_r, r_p = designParams
    rho_a, P_atm, C_d, C_r, mu_s, r_w, m_w, g = fixedParams
    wall, V0, V_t, A_p, L_p, m_p, m, A = computedParams
    
    #
    # Check Train Height & Length
    #
    
    # assume non-caboose
    trainHeight = 2*r_o + 2*r_p + r_w # diameter of tank + piston diameter + wheel radius
    trainLength = L_t # length of tank
    
    if trainLength > 1.5:
        # nothing will work tank is too long. 
        raise Error("Tank is too long. L_t should be in range (0.2, 1.5)")
    
    if trainHeight > 0.23:
        # non-caboose no work, try caboose
        trainHeight = 2*r_o + r_w
        trainLength = L_t + L_p
        
        if not (trainHeight <= 0.23 and trainLength <= 1.5): # if train dims do NOT meet req
            # invalid params
            raise Error("Invalid Train Height {:8.2g} m, Width {:8.2g} m".format(trainHeight, trainLength))
    
    #
    # Check Gear Radius
    #
    
    # the radius of the pinion gear must be less than the radius of the train wheel (i.e., rg/rw < 1).
    if not (r_g/r_w < 1):
        raise Error("Pinion gear is too large")
    
    # Yaay! we passed all the checks
    return