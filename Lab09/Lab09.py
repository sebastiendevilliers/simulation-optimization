#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 13 17:58:52 2020

@author: sebastiendevilliers
"""

import numpy as np
import matplotlib.pyplot as plt
# Your code will need to check for wheel-slip criterion in Equation 9 and print an error message if it is violated.


def runge_kutta_four(odefun, tspan, y0):
    """
    implements 4th Order Runge Kutta Method for an Nth order ODE

    Parameters
    ----------
    odefun : function 
        A function handle referring to the ODE function we want to solve.
        Parameters: 
            - t: scalar
            - y: np.array of size (m,)
        Returns: 
            - [dy[i]/dt for i in range(len(y))]
        NOTE: y.shape = y0.shape = (m,)
        
    tspan : np.array of size (n,)
        Time values of integration. 
        
        The elements in tspan must be all increasing.
        The solver imposes the initial conditions given by y0 at the initial 
        time given in the f􏲀irst entry of tspan, then integrates from the f􏲀irst 
        to the last entry in tspan.
        
    y0 : np.array of size (m,)
        Initial conditions.
        
        y0 contains the initial conditions for the equation 
        def􏲀ined in odefun. Remember that for higher order ODEs, the order
        of y0 should match the y-input for odefun. For a 1st order ODE just 
        pass in an np.array with one element in it


    Returns
    -------
    x : np.array of size (n,)
        the same as tspan.
    y : np.array of size (n,m)
        rows correspond to different times, columns corespond to the different 
        ODE equations. Columns ordered like y0.

    """
    # set up x and y
    x = tspan;
    y = np.zeros((len(x), len(y0)), 'float');
    y[0] = y0;
    # step through x values
    for i in range(len(x)-1):
        h = x[i+1] - x[i] # variable step size
        
        k1 = odefun(x[i]        , y[i]           ) # eulers aka inital
        k2 = odefun(x[i] + 0.5*h, y[i] + 0.5*k1*h) # midpoint
        k3 = odefun(x[i] + 0.5*h, y[i] + 0.5*k2*h) # midpoint update
        k4 = odefun(x[i] +     h, y[i] +     k3*h) # final
        
        y[i+1] = y[i] + 1/6*(k1 + 2*k2 + 2*k3 + k4) * h; # caculate next y
        
    return x, y

def train_motion(t, y, params):
    """
    Evaluates the ODE's governing the train's motion. Switches from deceleration
    to acceleration when the piston has fully extended
    
    Parameters
    ----------
    t : float
        Current Time.
    y : float vector
        State [position, velocity] of the moving train at time t. 
    params : array
        Array of system properties. see driver script for variable descriptions

    Returns
    -------
    float vector
        The result of evaluating the two f􏳉irst-order ODEs at time t. 
        [velocity, acceleration]

    """
    # see driver script for variable descriptions
    L_s, r_w, r_g, r_p, g, m_w, P_gauge, rho, m, A, mu_s, C_p, C_r = params
    # area of the piston
    A_p = np.pi * r_p**2
    # position, velocity
    x, v = y

    dxdt = v
    
    if y[0] < L_s * r_w / r_g:
        # propulsion stage  =>  m * dvdt = Ft - Fd - Fr
        dvdt = (r_g*P_gauge*A_p/r_w - 0.5*C_p*rho*A*v**2 - C_r*m*g) / (m + m_w)
    else:
        # deceleration stage  =>  m * dvdt =    - Fd - Fr
        dvdt = (- 0.5*C_p*rho*A*v**2 - C_r*m*g) / m
        
    # check for wheel slip
    Ft = r_g*P_gauge*A_p/r_w - m_w*dvdt
    if Ft > mu_s*m/2*g: raise RuntimeError("Wheel Slip Occurred!")
    
    dydt = np.array([dxdt, dvdt])
    return dydt

#%% Driver Script

# solve the system of two f􏳉irst-order ODEs. 
# The output of this program should be two plots:

L_s = 0.1 # • Piston stroke length: 0.1 m 
r_w = 0.025 # • Wheel radius: 2.5 cm
r_g = 0.01 # • Gear radius: 1.0 cm
r_p = 0.01 # • Piston radius: 1.0 cm
g = 9.81 # • Acceleration of gravity: 9.81 m/s2 
m_w = 0.1 # • Wheel mass: 0.1 kg
P_gauge = 100000 # • Tank gauge pressure: 100 kPa
rho = 1.0 # • Air density: 1.0 kg/m3
m = 10 # • Train mass: 10 kg
A = 0.05 # • Total frontal area of train: 0.05 m2
mu_s = 0.7 # • Coef􏰁icient of static friction: 0.7
C_p = 0.8 # • Drag coef􏰁icient: 0.8
C_r = 0.03 # • Rolling resistance coef􏰁icient: 0.03
y0 = np.array([0,0]) # • Initial conditions on train: x0 = 0 and v0 = 0.

params = np.array([L_s, r_w, r_g, r_p, g, m_w, P_gauge, rho, m, A, mu_s, C_p, C_r])

y0 = np.array([0, 0]) # • Initial conditions on train: x0 = 0 and v0 = 0.

def odefun(t, y):
    return train_motion(t, y, params)

tspan = np.linspace(0, 3, 100)

t,result = runge_kutta_four(odefun, tspan, y0)
position = result[:,0]
velocity = result[:,1]

# 1. position of the train versus time and
plt.figure()
plt.plot(t, position)
plt.title("Train Position vs. Time")
plt.xlabel("Time (s)")
plt.ylabel("Position (m)")

# 2. velocity of the train versus time.
plt.figure()
plt.plot(t, velocity)
plt.title("Train Velocity vs. Time")
plt.xlabel("Time (s)")
plt.ylabel("Velocity (m)")